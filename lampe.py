# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

import machine
import network
import ntptime
import time
import config
from machine import TouchPad, Pin
import esp32
from neopixel import NeoPixel
import timeslot


TOUCH_PIN_RESET = 13
TOUCH_PIN_HALT = 33
TOUCH_THRESHOLD = 400  # rigorous scientific experimentation arrived at this value

_touch_reset = None
_touch_halt = None

CURRENT_COLOR = (0, 0, 0)
DEFAULT_FADE_CYCLES = 1800
pin = None
np = None


def log(*args, **kwargs):
    if config.debug:
        print(*args, **kwargs)


def get_touchpad():
    global _touch_reset
    global _touch_halt
    if _touch_halt is None:
        _touch_halt = TouchPad(Pin(TOUCH_PIN_HALT))
    if _touch_reset is None:
        _touch_reset = TouchPad(Pin(TOUCH_PIN_RESET))

    return _touch_halt, _touch_reset


def check_touchpin():
    """Check the state of the GPIO13 touch pin."""

    touch_halt, touch_reset = get_touchpad()
    print(touch_halt.read())
    return touch_halt.read() < TOUCH_THRESHOLD


def do_connect():
    """Create a Wifi connection"""

    colors = [
        (0, 0, 255),
        (0, 0, 255),
        (0, 0, 255),
        (0, 0, 255),
        (0, 0, 255),
        (0, 0, 255),
        (0, 0, 100),
        (0, 0, 100),
        (0, 0, 100),
        (0, 0, 100),
        (0, 0, 100),
        (0, 0, 100),
    ]
    ln = len(colors)
    index = [0]

    def toggle():
        ind = index[0]
        np[0] = colors[ind]
        np.write()
        ind = (ind + 1) % ln
        index[0] = ind

    def error():
        np[0] = (100, 0, 0)
        np.write()
        return

    log("connecting")
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        # print("connecting to network...")
        sta_if.active(True)
        sta_if.config(reconnects=0)
        sta_if.connect(config.ssid, config.password)
        toggle()
        while not sta_if.isconnected():
            time.sleep_ms(100)
            toggle()
            status = sta_if.status()
            if status == network.STAT_ASSOC_FAIL:
                print("ASSOC FAIL")
                error()
                return
            if status == network.STAT_HANDSHAKE_TIMEOUT:
                print("HANDSHAKE TIMEOUT")
                error()
                return
            if status == network.STAT_WRONG_PASSWORD:
                print("WRONG PASSWORD")
                error()
                return
            if status == network.STAT_BEACON_TIMEOUT:
                print("BEACON TIMEOUT")
                error()
                return

    np[0] = (0, 0, 0)
    np.write()
    log("network config:", sta_if.ifconfig())


def disconnect_wifi():
    np[0] = (20, 20, 0)
    np.write()
    wlan = network.WLAN(network.STA_IF)
    wlan.active(False)
    np[0] = (0, 0, 0)
    np.write()


def set_time():
    """Set time from NTP."""
    log("setting time...")
    ntptime.host = config.ntp_host
    ntptime.settime()
    log("time is", time.localtime())


def set_color(c):
    global CURRENT_COLOR
    CURRENT_COLOR = c
    for i in range(config.LIGHT_SIZE):
        np[i] = CURRENT_COLOR
    np.write()


def main_start():
    """Main start: called once on hard start."""

    global np

    pin = Pin(config.DATA_PIN, Pin.OUT)
    np = NeoPixel(pin, config.LIGHT_SIZE)

    for i in range(config.LIGHT_SIZE):
        np[i] = (0, 0, 0)
    np[0] = (0, 255, 0)
    np.write()

    try:
        do_connect()
        set_time()
        disconnect_wifi()
    except Exception as e:
        log(e)
        retry()


def main():
    """Do what is necessary after waking up from deep sleep."""

    slot, from_time, color, until_time = timeslot.find_slot()
    fade_to(color)

    curr_time, _ = timeslot.get_curr_time()
    sleep_seconds = timeslot.seconds_until(curr_time, until_time)
    log("next switch is at", until_time)
    log("sleeping for", sleep_seconds, "seconds")
    if sleep_seconds > 0:
        time.sleep(sleep_seconds)


def fade_to(to_color, cycles=DEFAULT_FADE_CYCLES):
    if CURRENT_COLOR == to_color:
        return
    log("fading to color:", to_color)
    from_color = CURRENT_COLOR
    for i in range(cycles):
        factor = i / cycles
        color = (
            int(from_color[0] * (1 - factor) + to_color[0] * factor),
            int(from_color[1] * (1 - factor) + to_color[1] * factor),
            int(from_color[2] * (1 - factor) + to_color[2] * factor),
        )

        # cool dithering...
        error_per_pixel = (
            from_color[0] * (1 - factor) + to_color[0] * factor - color[0],
            from_color[1] * (1 - factor) + to_color[1] * factor - color[1],
            from_color[2] * (1 - factor) + to_color[2] * factor - color[2],
        )
        error = [0, 0, 0]
        for i in range(config.LIGHT_SIZE):
            color_pixel = list(color)
            for j in (0, 1, 2):
                error[j] += error_per_pixel[j]
                if error[j] > 0.5:
                    color_pixel[j] += 1
                    error[j] -= 1
                elif error[j] < -0.5:
                    color_pixel[j] -= 1
                    error[j] += 1
            np[i] = color_pixel

        np.write()

        time.sleep_ms(50)
    set_color(to_color)


def retry():
    """If any error occurs, we simply retry in ten minutes."""
    log("an error occurred, re-trying shortly")
    _, tp = get_touchpad()
    tp.config(TOUCH_THRESHOLD)
    esp32.wake_on_touch(True)
    machine.deepsleep(10 * 1000)


def go_to_sleep():
    """Go to sleep again. Depending on time of day, we use a different sleep delay."""

    log("going to sleep...")

    log("sleeping for", config.morning_delay)
    # deepsleep(config.morning_delay)


def deepsleep(delay):
    """Actually go to sleep!"""
    _, tp = get_touchpad()
    tp.config(TOUCH_THRESHOLD)
    esp32.wake_on_touch(True)

    # disable wifi
    log("  disabling wifi...")
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(False)

    log("  sleeping")
    machine.deepsleep(delay)


def start():
    try:
        if check_touchpin():
            print("stopping because of touched pin, reset to... uh... restart!")
            pass
        else:
            log("starting...")
            main_start()
            while True:
                main()
    except Exception as e:
        # this is the hardest retry we can do: let's go for ten seconds
        log(e)
        machine.deepsleep(10 * 1000)
