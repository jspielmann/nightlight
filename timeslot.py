import time

import config


def get_curr_time():
    localtime = time.localtime()
    (_, _, _, tm_hour, tm_min, tm_sec, weekday, _) = localtime
    tm_hour = (tm_hour + config.timezone_offset) % 24
    curr_time = (tm_hour, tm_min, tm_sec)
    return curr_time, weekday


def find_slot(curr_time=None):
    localtime, weekday = get_curr_time() if curr_time is None else curr_time

    conf = config.timetable[weekday]

    for i in range(len(conf)-1):
        slot_time, slot_color = conf[i]
        next_slot_time, _ = conf[i + 1]
        if slot_time <= localtime < next_slot_time:
            return i, slot_time, slot_color, next_slot_time

    return conf


def seconds_until(curr_time, target_time):
    """
    :param curr_time: (curr_h, curr_m, curr_s)
    :param target_time: (target_h, target_m, target_s)
    :return: seconds from now until target time
    """

    curr_seconds = curr_time[0] * 3600 + curr_time[1] * 60 + curr_time[2]
    target_seconds = target_time[0] * 3600 + target_time[1] * 60 + target_time[2]

    return target_seconds - curr_seconds


if __name__ == '__main__':

    curr_t = ((20, 30, 0), 6)
    print(find_slot(curr_t))

    _, _, _, target_time = find_slot(curr_t)

    print(seconds_until(curr_t[0], target_time))
