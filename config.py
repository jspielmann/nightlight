"""
Configuration for your display: change these settings to fit your environment and choices.

Copy this file to `config.py`, then change the values as appropriate. Then copy to your frame.
"""
# Copyright 2021 Johannes Spielmann
# This file is part of epd-pictureframe.
#
#     epd-pictureframe is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     epd-pictureframe is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with epd-pictureframe.  If not, see <https://www.gnu.org/licenses/>.

# Do you want to see debug output on the serial console?
debug = True

# the wifi name and password this display should connect to
ssid = ""
password = ""

# how long do you want to sleep between checks
sleep_delay = 10

# data pin
DATA_PIN = 25
LIGHT_SIZE = 31

# local time
timezone_offset = 1

colors = {
    'off': (0, 0, 0),
    'bright orange': (249, 130, 35),
    'slightly red': (15, 3, 0),
    'dark red': (8, 1, 0),
    'dark orange': (10, 5, 0),
    'orange': (40, 20, 0),
    'bright green': (35, 249, 61),
    'dark green': (1, 10, 2),
    'green': (5, 50, 10),
}

# format: (time h, m, s), (color r, g, b)
timetable = [
    (   # Mo
        ((0, 0, 0), colors['dark red']),
        ((6, 0, 0), colors['dark green']),
        ((7, 0, 0), colors['green']),
        ((8, 0, 0), colors['off']),
        ((19, 30, 0), colors['slightly red']),
        ((21, 0, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
    (  # Di
        ((0, 0, 0), colors['dark red']),
        ((6, 0, 0), colors['dark green']),
        ((7, 0, 0), colors['green']),
        ((8, 0, 0), colors['off']),
        ((19, 30, 0), colors['slightly red']),
        ((21, 0, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
    (  # Mi
        ((0, 0, 0), colors['dark red']),
        ((6, 0, 0), colors['dark green']),
        ((7, 0, 0), colors['green']),
        ((8, 0, 0), colors['off']),
        ((19, 30, 0), colors['slightly red']),
        ((21, 0, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
    (  # Do
        ((0, 0, 0), colors['dark red']),
        ((6, 0, 0), colors['dark green']),
        ((7, 0, 0), colors['green']),
        ((8, 0, 0), colors['off']),
        ((19, 30, 0), colors['slightly red']),
        ((21, 0, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
    (  # Fr
        ((0, 0, 0), colors['dark red']),
        ((6, 0, 0), colors['dark green']),
        ((7, 0, 0), colors['green']),
        ((8, 0, 0), colors['off']),
        ((19, 30, 0), colors['slightly red']),
        ((21, 30, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
    (  # Sa
        ((0, 0, 0), colors['dark red']),
        ((7, 0, 0), colors['dark green']),
        ((8, 0, 0), colors['green']),
        ((8, 15, 0), colors['off']),
        ((20, 00, 0), colors['slightly red']),
        ((21, 30, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
    (  # So
        ((0, 0, 0), colors['dark red']),
        ((7, 0, 0), colors['dark green']),
        ((8, 0, 0), colors['green']),
        ((8, 15, 0), colors['off']),
        ((19, 30, 0), colors['slightly red']),
        ((21, 00, 0), colors['dark red']),
        ((24, 0, 0), colors['dark red']),
    ),
]

# night times
times = [
    {  # Mo
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (6, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
    {  # Di
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (6, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
    {  # Mi
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (6, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
    {  # Do
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (6, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
    {  # Fr
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (6, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
    {  # Sa
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (7, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
    {  # So
        'night_start': (19, 30, 0),
        'night_switchdown_time': (21, 0, 0),
        'night_end': (7, 0, 0),
        'morning_end': (8, 0, 0),
        'morning_end_sleep': (8, 10, 0),
    },
]

# how long should we go to deep sleep in the morning?
morning_delay = 11 * 60 * 60 + 1700  # ~11.5 hours

# colors
# color_night = (249, 130, 35)
# color_night = (40, 20, 0)
color_night = (10, 5, 0)
color_switchdown = (10, 5, 0)
# color_morning = (35, 249, 61)
# color_morning = (5, 50, 10)
color_morning = (1, 10, 2)


# this is a good NTP host -- we will only use it once anyway, our RTC is quite good enough!
ntp_host = "time.fu-berlin.de"
